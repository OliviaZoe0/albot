
/**
 * Created by nexus on 03/04/17.
 */
process.on('uncaughtException', function(exception) {
    console.log(exception);
    console.log(exception.stack);
});

var LocalStorage = require('node-localstorage').LocalStorage;
var HttpWrapper = require("./httpWrapper");
localStorage = new LocalStorage('./localStorage');

function close(error) {
    console.error(error);
    process.exit(1);
}


var Game = function(ip, port, characterId, script, botKey, G, httpWrapper) {
    this.ip = ip;
    this.port = port;
    this.userId = httpWrapper.userId;
    this.characterId = characterId;
    this.socketAuth = httpWrapper.userAuth;
    this.httpWrapper = httpWrapper;
    this.script = script;
    this.botKey = botKey;
    this.excutor = null;
    this.interface = null;
    this.events = {};
    this.socket = null;
    this.executor = null;
    this.G = G;
    this.pathfinding = null;
}

Game.prototype.init = function() {
    var cpuStat = require('cpu-stat');
    let self = this;
    var os = require("os")
    var fs = require("fs")
    var cheerio = require("cheerio");
    var G = this.G;
    var Executor = require("./Executor");
  const { PerformanceObserver, performance } = require('perf_hooks');
    var character_to_load;
    var first_entities = false;
    var inside = "selection";
    var user_id, user_auth;
    var server_names = {
        "US": "Americas",
        "EU": "Europas",
        "ASIA": "Eastlands"
    };
    var perfect_pixels = '';
    var cached_map = '1',
        scale = '2';
    var d_lines = '1';
    var sd_lines = '1';
    var c_enabled = '1',
        stripe_enabled = '';
    var auto_reload = "auto",
        reload_times = '0',
        code_to_load = null,
        mstand_to_load = null;
    var EPS = 1e-16;
    var no_graphics = true;
    var first_coords = false,
        first_x = 0,
        first_y = 0;
    var protocol = "https";

    var code_active = false;
    var current_map = "";
    var pull_all_next = false;
    var pull_all = false;
    var heartbeat = new Date();
    var slow_heartbeats = 0;
    var game_loaded = false;
    var prepull_target_id = null;
    var is_pvp = false;
    var server_region = "";
    var server_identifier = "";
    var server_name = "";
    var socket;
    var server_addr, port;
    var last_draw = new Date();
    var M;
    var GMO;
    var entities = {}
    var future_entities = {
        players: {},
        monsters: {}
    };
  var pings = [];
  var pingts = {};
  var S = {}
    var character;

    var game = null;

    var httpWrapper = this.httpWrapper;
    var script = this.script;
    var botKey = this.botKey;
    var sandbox;
  var lostandfound;

    game = this;

    server_addr = this.ip;
    port = this.port;
    user_id = this.userId;
    character_to_load = this.characterId;
    user_auth = this.socketAuth;
  function no_no_no() {
  }
    var onLoad = function() {
        log_in(user_id, character_to_load, user_auth);
    }

    eval(fs.readFileSync('modedGameFiles/game.js') + '');
    gprocess_game_data();
    init_socket();
    this.socket = socket;
  var on_cm_handler = function() {};
    /*
    game.pathfinding = require("./PathFinding/pathFinding");
    game.pathfinding.initialize(this.G);
    */
    var glob = Object.create(global, Object.getOwnPropertyDescriptors({
        trade_sell: trade_sell,
        calculate_item_value: calculate_item_value,
        localStorage: localStorage,
        gameplay: gameplay,
        get is_pvp() { return is_pvp },
        ping: ping,
        get server_region() { return server_region },
        get server_identifier() {return server_identifier},
        G: G,
        get S() { return S },
      get lostandfound() { return lostandfound},
        get party() { return party},
        activate: activate,
        shift: shift,
        performance: performance,
        use_skill: use_skill,
      donate: function donate(a) { socket.emit("donate",{gold:a})},
        can_use: can_use,
        socket: socket,
        current_map: current_map,
        server_addr: server_addr,
        add_log: add_log,
        ctarget: ctarget,
      eval: eval,
      get next_skill() { return next_skill},
        send_target_logic: send_target_logic,
        distance: distance,
        is_disabled: is_disabled,
        transporting: transporting,
        player_attack: player_attack,
        monster_attack: monster_attack,
        player_heal: player_heal,
        buy: buy,
      set_on_cm_handler: function() {
        on_cm_handler = arguments[0];
      },
      
        no_graphics: true,
        sell: sell,
        trade: trade,
        trade_buy: trade_buy,
        upgrade: upgrade,
        compound: compound,
        exchange: exchange,
        say: say,
        calculate_move: calculate_move,
        chests: chests,
        entities: entities,
        calculate_vxy: calculate_vxy,
        show_json: show_json,
        next_potion: next_potion,
        send_code_message: function send_code_message(a, b) {
            if (!is_array(a)) {
                a = [a];
            }
            process.send({
                event: "cm",
                names: a,
                message: b
            })
        },
        drawings: drawings,
        move: move,
        show_modal: show_modal,
        prop_cache: prop_cache,
        next_attack: next_attack,
        bot_mode: true,
        botKey: botKey,
        game: this,
        get code_active() {
            return code_active
        },
        set code_active(value) {
            code_active = Boolean(value)
        },
        get sandbox() {
            return sandbox;
        },
        set sandbox(value) {
            sandbox = value;
        }
    }));
    glob.emit_stoneworm = function() {
        process.send({
            event: "script_switch",
            value: "stoneworm"
        })
    }
  glob.emit_boar = function() {
        process.send({
            event: "script_switch",
            value: "boar"
        })
    }
    glob.emit_dracul = function() {
        process.send({
            event: "script_switch",
            value: "dracul"
        })
    }
    glob.emit_jr = function() {
        process.send({
            event: "script_switch",
            value: "jr"
        })
    }
    glob.emit_solo = function() {
        process.send({
            event: "script_switch",
            value: "solo"
        })
    }
    glob.emit_skeletor = function() {
        process.send({
            event: "script_switch",
            value: "skeletor"
        })
    }
    glob.emit_rat = function() {
        process.send({
            event: "script_switch",
            value: "rat"
        })
    }
  glob.emit_bbpompom = function() {
        process.send({
            event: "script_switch",
            value: "bbpompom"
        })
    }
    glob.emit_prat = function() {
        process.send({
            event: "script_switch",
            value: "prat"
        })
    }
  glob.emit_xscorpion = function() {
        process.send({
            event: "script_switch",
            value: "xscorpion"
        })
    };
  glob.emit_hawk = function() {
        process.send({
            event: "script_switch",
            value: "hawk"
        })
    }
  glob.skill_timeout = function skill_timeout(c, b) {
    if (b <= 0) {
        return
    }
    var a = [];
    if (!b) {
        b = G.skills[c].cooldown
    }
    if (b == "1X") {
        b = 1000 * 100 / character.speed
    }
    next_skill[c] = future_ms(b);
}
    glob.emit_xscorpion = function () {
        process.send({
            event: "script_switch",
            value: "xscorpion"
        })
    }
    glob.switch_server = function(server) {
        process.send({
            action: "server",
            data: server
        })
        glob.socket.emit("say", {
            message: `Switching servers!`,
            code: 0
        });
        self.emit("disconnected", "nothing");
        self.stop();
        process.send({
            type: "status",
            status: "disconnected"
        });
    }
	Object.defineProperty(glob, "load_lib", {
		get: function() {
			return function load_lib(lib_name, eval) {
				eval(fs.readFileSync('LIB/' + lib_name) + '')
			}
		}
	})
    Object.defineProperty(glob, "entities", {
        get: function() {
            return entities;
        }
    })
    Object.defineProperty(glob, "code_active", {
        get: function() {
            return code_active;
        },
        set: function(value) {
            code_active = value;
        }
    });
	Object.defineProperty(glob, "shutdown", {
        get: function() {
            return function() {
              process.exit()
            }
        },
        set: function(value) {
        }
    })
    Object.defineProperty(glob, "sandbox", {
        get: function() {
            return sandbox;
        },
        set: function(value) {
            sandbox = value;
        }
    })
    Object.defineProperty(glob, "character", {
        get: function() {
            return character;
        }
    })
    Object.defineProperty(glob, "map", {
        get: function() {
            return map;
        }
    })
    Object.defineProperty(glob, "M", {
        get: function() {
            return M;
        }
    })
    var damage = 0;
    var timeFrame = 60 * 5;
    var goldTimeline = [],
        xpTimeline = [],
        damageTimeline = [];

    var damageStart = Date.now();
    socket.on("hit", function(data) {
        if (data.hid && data.damage && character) {
            if (data.hid == character.id) {
                damage += data.damage;
            }
        }
    })
    process.on("message", function(message) {
        if (message.event == "cm_fail") {
            send_code_message(message.name, message.message)
        } else if (message.event == "cm") {
          socket.listeners('cm').forEach((listener) => {
          	listener({name: message.sender, message: JSON.stringify(message.message)})
          })
          on_cm_handler(message.name, message.message)
            call_code_function("on_cm", message.sender, message.message)
        } else if (socket.connected && character != null && character.name == message.name) {
            if (message.admin) {
                if (message.action == 'move_item') {
                    socket.emit("imove", {
                        a: message.a,
                        b: message.b
                    });
                }
            } else {
                if (message.action == 'get_items') {
                    process.send({
                        type: "socket_response",
                        value: character.items,
                        client: message.client
                    });
                }
            }
        }
    })
    socket.on("start", function() {
        let start_gold = character.gold;
        let maxDPS = 1,
            bank = {
                gold: "N/A"
            },
            maxGPH = 1;
        let goldInterval = 1000 * 60 * 60;
        let goldLog = [];
        parent.prev_handlersgoldmeter = [];

        function getGold() {
            var sumGold = 0;
            var minTime;
            var maxTime;
            var entries = 0;

            for (id in goldLog) {
                logEntry = goldLog[id];
                if (new Date() - logEntry.time < goldInterval) {
                    if (minTime == null || logEntry.time < minTime) {
                        minTime = logEntry.time;
                    }

                    if (maxTime == null || logEntry.time > maxTime) {
                        maxTime = logEntry.time;
                    }

                    sumGold += logEntry.gold;
                    entries++;
                } else {
                    goldLog.splice(id, 1);
                }
            }

            if (entries <= 1) {
                return 0;
            }

            var elapsed = maxTime - minTime;

            var goldPerSecond = parseFloat(Math.round((sumGold / (elapsed / 1000)) * 100) / 100);

            return parseInt(goldPerSecond * 60 * 60)
        }

        function get_cc() {
            var fillAmount = ((character.cc / 180) * 100).toFixed(0);
        }

        function register_goldmeterhandler(event, handler) {
            parent.prev_handlersgoldmeter.push([event, handler]);
            socket.on(event, handler);
        };

        function goldMeterGameResponseHandler(event) {
            if (event.response == "gold_received") {
                var goldEvent = Object.create(null);
                goldEvent.gold = event.gold;
                goldEvent.time = new Date();
                goldLog.push(goldEvent);
            }
        }

        function goldMeterGameLogHandler(event) {
            if (event.color == "gold") {
                var gold = parseInt(event.message.replace(" gold", "").replace(",", ""));

                var goldEvent = Object.create(null);
                goldEvent.gold = gold;
                goldEvent.time = new Date();
                goldLog.push(goldEvent);
            }
        }
      var tracker = {};
      socket.on("tracker", (t) => {
        tracker = t;
})
        var minute_refresh; // how long before the clock resets
        var last_minutes_checked = new Date();
        var last_xp_checked_minutes = character.xp;
        var last_xp_checked_kill = character.xp;
        // lxc_minutes = xp after {minute_refresh} min has passed, lxc_kill = xp after a kill (the timer updates after each kill)
        let last = null;

        function update_xptimer() {
            if (character.xp == last_xp_checked_kill) return last;

            let $ = parent.$;
            let now = new Date();

            let time = Math.round((now.getTime() - last_minutes_checked.getTime()) / 1000);
            if (time < 1) return; // 1s safe delay
            let xp_rate = Math.round((character.xp - last_xp_checked_minutes) / time);
            if (time > 60 * minute_refresh) {
                last_minutes_checked = new Date();
                last_xp_checked_minutes = character.xp;
            }
            last_xp_checked_kill = character.xp;

            let xp_missing = parent.G.levels[character.level] - character.xp;
            let seconds = Math.round(xp_missing / xp_rate);
            let minutes = Math.round(seconds / 60);
            let hours = Math.floor(minutes / 60);
            let days = Math.floor(hours / 24);
            let counter = `${days}d ${hours % 24}h ${minutes % 60}min`;
            return last = [ncomma(xp_rate), counter];
        }

        function ncomma(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        register_goldmeterhandler("game_log", goldMeterGameLogHandler);
        register_goldmeterhandler("game_response", goldMeterGameResponseHandler);
      let cpu_usage;  
      setInterval(() => {
      
                socket.emit("tracker");
      }, 5000)
      setTimeout(function() {
            setInterval(function() {
                cpuStat.usagePercent(function(err, percent, seconds) {
                    if (err) {
                      return console.log(err);
                    }
                    cpu_usage = percent;
                });
                var targetName = "nothing";
                if (character.target && entities[character.target]) {
                    if (entities[character.target].player) {
                        targetName = entities[character.target].id
                    } else {
                        targetName = entities[character.target].mtype + ' +' + (entities[character.target].level || 1);
                    }
                }
                let dps = Math.floor((damage * 100000) / (Date.now() - damageStart)) / 100;
                let goldph = getGold();
                if (dps > maxDPS) {
                    maxDPS = dps
                }
                if (goldph > maxGPH) {
                    maxGPH = goldph
                }

                function num_items(name) {
                    return character.items.filter((item) => {
                        return item != null && item.name == name
                    }).reduce((a, b) => {
                        return a + (b.q || 1)
                    }, 0)
                }
                bank = character.bank || bank;
                let time = new Date()
                let [xpPS, remaining] = update_xptimer() || [0, 0];
              let memory = process.memoryUsage();
                process.send({
                    type: "bwiUpdate",
                    data: {
                      script: script,
                        name: character.id,
                        gold: character.gold.toLocaleString('en'),
                        level: character.level,
                        inv: character.isize - character.esize + " / " + character.isize,
                        xp: Math.floor(character.xp * 10000 / character.max_xp) / 100,
                        health: [character.hp, Math.floor(character.hp * 10000 / character.max_hp) / 100],
                        mana: [character.mp, Math.floor(character.mp * 10000 / character.max_mp) / 100],
                        target: targetName + (character.target ? (character.target.level > 1 ? (' +' + character.target.level) : '') : ''),
                        goldh: [goldph.toLocaleString('en'), goldph / maxGPH * 100],
                        status: character.rip ? "Dead" : "Alive",
                        dps: [dps, dps / maxDPS * 100],
                        server: glob.server_region + " " + glob.server_identifier,
                        mp: num_items("mpot1"),
                        tilLevelUp: remaining,
                        cc: Math.floor(character.cc * 100 / 1.8) / 100,
                        xpPS: xpPS.toLocaleString('en'),
                        lastSeen: time.toLocaleString(),
                        bank: Object.assign({
                            key: character.name
                        }, bank),
                        bank_gold: bank.gold,
                        inventory: Object.assign({
                            key: character.name
                        }, character.items),
                        map: character.map,
                        ping: ~~character.ping,
                      tracker: Object.assign({
                        key: character.name
                      }, tracker),
                      cpu: [Math.floor(cpu_usage * 100)/100, cpu_usage],
                      heap: [Math.trunc(memory.heapUsed / (1024 ** 2) * 100) / 100 + "MB", memory.heapUsed/memory.heapTotal * 100]
                    }
                });
            }, 1000);


            self.executor = new Executor(glob, script);
            glob.load_script = self.executor.load_code;
            self.executor.execute();
            code_active = false;
        }, 3000)
    });
    socket.on("disconnect", function() {
        self.emit("disconnected", "nothing");
        process.send({
            type: "status",
            status: "disconnected"
        });
        self.stop();
    });
    socket.on("game_error", function(data) {
        if ("Failed: ingame" == data) {
            setTimeout(function() {
                console.log("Retrying for " + character_to_load);
                log_in(user_id, character_to_load, user_auth);
            }, 30 * 1000);
        } else if (/Failed: wait_(\d+)_seconds/g.exec(data) != null) {
            let time = /Failed: wait_(\d+)_seconds/g.exec(data)[1];
            setTimeout(function() {
                console.log("Retrying for " + character_to_load);
                log_in(user_id, character_to_load, user_auth);
            }, time * 1000 + 1000);
        }
    });
}
/**
 * Register's an event in the game
 * @param event string the name f the event
 * @param callback function the function to be called
 */
Game.prototype.on = function(event, callback) {
    if (typeof event == "string" && typeof callback == "function") {
        if (!this.events[event]) {
            this.events[event] = [];
        }
        this.events[event].push(callback);
    } else {
        if (typeof event != "string")
            throw new Error("Event has to be a string")
        if (typeof callback == "function")
            throw new Error("Callback has to be a function")
    }
};
Game.prototype.trigger=function(event,args){
	var to_delete=[];
  for(var i=0;i<game.listeners.length;i++)
	{
		var l=game.listeners[i];
		if(l.event==event || l.event=="all")
		{
			try{
				if(l.event=="all") l.f(event,args)
				else l.f(args,event);
			}
			catch(e)
			{
				game_log("Listener Exception ("+l.event+") "+e,colors.code_error);
			}
			if(l.once || l.f && l.f.delete) to_delete.push(l.id);
		}
	}
	// game_log(to_delete);
};
 
Game.prototype.emit = function(event, arguments) {
    if (typeof event == "string") {
        if (this.events[event]) {
            this.events[event].forEach(function(current) {
                current.apply(Array.from(arguments).slice(1))
            });
        }
    }
}

Game.prototype.stop = function() {
    if (this.socket)
        this.socket.close();
};
async function main() {
    try {
        let args = process.argv.slice(2);
        let httpWrapper = new HttpWrapper(args[0], args[1], args[2]);
        let gameData = await httpWrapper.getGameData();
        let game = new Game(args[3], args[4], args[5], args[6], args[7], gameData, httpWrapper);
        game.init();
    } catch (e) {
        console.log(e)
    }
}

main();