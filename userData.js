let script = "XScorpion.js",  
    server = "EU II";
module.exports = {
  "config": {
    "fetch": false,
    "botKey": 1,
    "botWebInterface": {
      "start": true,
      "port": 3000,
    }
  },
  "login": {  
    "email": process.env.username,
    "password": process.env.password
  },
  "bots":  [
    /* {
        "characterName": "AriaHarper",
        "characterId": "5513766260178944",
        "runScript": "MerchantSolo.js",
        "server": server
    }, // */
    {
        "characterName": "Geoffriel",
        "characterId": "5178085749030912",
        "runScript": script,
        "server": server
    }, // */
    {
        "characterName": "Rael",
        "characterId": "6011174247202816",
        "runScript": script,
        "server": server
    }, // */
    /* {
        "characterName": "Firenus",
        "characterId": "6389196039127040",
        "runScript": script,
        "server": server
    }, // */ 
    {
        "characterName": "Boismon",
        "characterId": "5760997546524672",
        "runScript": script,
        "server": server
    }, // */
    /* {
        "characterName": "Daedulaus",
        "runScript": script,
        "server": server
    } // */
  ]
}