/**
 * Created by Nexus on 30.07.2017.
 */
var Controller = function () {
	this.botAmount = 0;
	this.dataIDs = null;
	this.structure = null;

	this.botUIs = {};
	this.socket = null;
};

Controller.prototype.start = function () {
	var self = this;
	var host = document.location.hostname;
	let protocol = document.location.protocol

	var socket = io(protocol + "//" + host, {
		autoConnect: false
	});
  console.log(socket)

	socket.on("connect", function () {
		socket.emit("auth", { token: "all" });
	});

	socket.on("setup", function (data) {
        /**
         * @typedef {object} data
         * @typedef {Array<int>} data.dataIDs
         * @typedef {Array<object>} data.structure;
         */
		console.log(data)
		self.dataList = data.dataList;
		self.structure = data.structure;

		for (let i in self.dataList) {
			var botUI = new BotUi(i, self.structure);
			botUI.create();
			self.botUIs[i] = botUI;
		}
		for (let i in self.dataList) {
			if (self.botUIs[i])
				self.botUIs[i].update(self.dataList[i]);
		}

	});

	socket.on("updateStructure", function (data) {
		//TODO: implement
		return;

		self.structure = data.structure;

		for (var j in self.botUIs) {

		}

		for (var i in self.dataIDs) {
			var botUI = new BotUi(self.dataIDs[i], self.structure);
			botUI.create();
			self.botUIs[self.dataIDs[i]] = botUI;
		}
	});
	socket.on('auth_success', function(data) {
		console.log(data)
	})

	socket.on("updateBotUI", function (data) {
      for (var i in data) {
        let botUI = self.botUIs[i]
        if (botUI) {
          botUI.update(data[i]);
        }
      }
	});

	socket.on("updateProperty", function (data) {
		if (self.botUIs[data.id])
			self.botUIs[data.id].updateProperty(data.name, data.value);
	});

	socket.on("queryResolved", function (data) {
		if (self.botUIs[data.id]) {
			self.botUIs[data.id].showQuery(data.value)
		}
	})

	socket.on("removeBotUI", function (data) {
		if (self.botUIs[data.id])
			self.botUIs[data.id].destroy();
	});

	socket.on("createBotUI", function (data) {
		var botUI = new BotUi(data.id, self.structure);
		botUI.create();
		self.botUIs[data.id] = botUI;
	});

	socket.open();
	this.socket = socket;
};
