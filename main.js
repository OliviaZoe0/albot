process.on('uncaughtException', function (exception) {
	console.log(exception);
	console.log(exception.stack);
});
let child_process = require("child_process"),
	HttpWrapper = require("./httpWrapper"),
	httpWrapper = new HttpWrapper(),
	BotWebInterface = require("./bot-web-interface"),
	fs = require("fs"),
	http = require("http"),
	userData = require("./userData.js"),
	login = userData.login,
	bots = userData.bots;
async function main() {
	var result = await httpWrapper.login(login.email, login.password),
        characters = await httpWrapper.getCharacters(),
        userAuth = await httpWrapper.getUserAuth();
	if (!result) {
		throw new Error("Login failed");  
	};

	if (userData.config.fetch) {
		console.log("Populating config file with data.");
		userData.bots = [];
		for (let i = 0, len = characters.length; i < len; i++) {
			userData.bots[i] = {
				characterName: characters[i].name,
				characterId: characters[i].id,
				runScript: "default.js",
				server: "EU I"
			};
		};
        userData.config.fetch = false;
    userData.login.password = userData.login.email = "not_saved";
    
		fs.writeFile("./userData.json", JSON.stringify(userData, null, 4), () => {
    		process.exit();
        });
	};

	//Checking for mistakes in userData.json
	if (!bots) {
		console.error("Missing field \"bots\" in userData.json");
	};

	for (let i = 0; i < bots.length; i++) {
		if (!(bots[i] && (bots[i].characterId || bots[i].characterName) && bots[i].runScript && bots[i].server)) {
			throw new Error("One or more necessary fields are missing from userData.json \n The following fields need to be present for a working executor:\n characterId or characterName\n runScript\n server\n");
        };
    };

	//Reverse lookup name to characterId, names can't be used for starting a bot.
	for (let i = 0; i < bots.length; i++) {
		if (!bots[i].characterId) {
			for (let j = 0; j < characters.length; j++) {
				if (bots[i].characterName === characters[j].name) {
					bots[i].characterId = characters[j].id;
				};
			};
		};
	};

	//Check that ids are unique, we don't want to start a bot twice.
	for (let i = 0, len = bots.length; i < len; i++) {
		if (bots[i]) {
			for (let j = i + 1; j < len; j++) {
				if (bots[j]) {
					if (bots[i].characterId === bots[j].characterId) {
						console.error("Duplicate characterId " + bots[i].characterId + " ignoring second declaration.");
						bots[j] = null;
					};
				};
			};
        };
	};

	let serverList = await httpWrapper.getServerList();
	if (userData.config.botWebInterface.start) {
		BotWebInterface.startOnPort(userData.config.botWebInterface.port);
		BotWebInterface.SocketServer.getPublisher().setStructure([
			{ name: "name", type: "text", label: "name" },
			{ name: "inv", type: "text", label: "Inventory" },
			{ name: "level", type: "text", label: "Level" },
			{ name: "xp", type: "progressBar", label: "Experience", options: { color: "green" } },
			{ name: "health", type: "outOfMax", label: "Health", options: { color: "red" } },
			{ name: "mana", type: "outOfMax", label: "Mana", options: { color: "blue" } },
			{ name: "goldh", type: "outOfMax", label: "Gold/H", options: { color: "gold" } },
			{ name: "dps", type: "outOfMax", label: "DPS", options: { color: "darkorange" } },
			{ name: "cc", type: "progressBar", label: "Code Cost" },
			{ name: "luckp", type: "outOfMax", label: "Luck Potions" },
			{ name: "target", type: "text", label: "Target" },
			{ name: "status", type: "text", label: "Status" },
			{ name: "gold", type: "text", label: "Gold" },
			{ name: "bank_gold", type: "text", label: "Bank Gold" },
			{ name: "server", type: "text", label: "Server" },
			{ name: "mp", type: "text", label: "Mana potions" },
			{ name: "xpPS", type: "text", label: "XP/S" },
			{ name: "tilLevelUp", type: "text", label: "ETLU" },
			{ name: "inventory", type: "object" },
			{ name: "tracker", type: "object" },
      { name: "script", type: "text", label: "Script" },
      { name: "ping", type: "text", label: "Ping" },
      { name: "map", type: "text", label: "Map" },
      { name: "cpu", type: "outOfMax", label: "CPU" },
      { name: "heap", type: "outOfMax", label: "memory" }
		]);
	};

	//Checks are done, starting bots.
	for (let i = 0; i < bots.length; i++) {
		let ip = "54.169.213.59",
            port = 8090;
		for (let j = 0; j < serverList.length; j++) {
			let server = serverList[j];
			if (bots[i].server === server.region + " " + server.name) {
				let { port: new_port, ip: new_ip } = server;
                [ip, port] = [new_ip, new_port];
			};
		};

		let args = [httpWrapper.sessionCookie, httpWrapper.userAuth, httpWrapper.userId, ip, port, bots[i].characterId, bots[i].runScript, userData.config.botKey];
		startGame(args, bots[i].characterName);
	};
};

const processes = new Map();

async function startGame(args, characterName, data = {}) {
	let serverList = await httpWrapper.getServerList(),
    childProcess = child_process.fork("./game", args ,{
		stdio: [0, 1, 2, 'ipc']
	});
	processes.set(characterName, childProcess);

	var botInterface = BotWebInterface.SocketServer.getPublisher().createInterface();
	botInterface.setDataSource(() => {
		return data;
	});

	let closed = false;
	BotWebInterface.SocketServer.on("command", (message) => {
		if (!closed) {
			childProcess.send(message);
		};
	});

	childProcess.on('message', (m) => {
		if(m.command == "shutdown") {
			[...processes.values()].forEach((p) => {
				p.kill()
			})
		}else if (m.type === "status" && m.status === "disconnected" && !closed) {
			childProcess.kill();
			BotWebInterface.SocketServer.getPublisher().removeInterface(botInterface);
			startGame(args, characterName, data);
			closed = true;
		} else if (m.type === "bwiUpdate") {
			data = m.data;
		} else if (m.type === "socket_response") {
			BotWebInterface.SocketServer.send_to_client(m.client, m.value)
		} else if (m.action == "server") {
			var ip, port;
			for (let j = 0; j < serverList.length; j++) {
				let server = serverList[j];
				if (m.data === server.region + " " + server.name) {
					ip = server.ip;
					port = server.port;
				};
			};
			args[3] = ip;
			args[4] = port;
		} else if (m.event == "cm") {
			m.names.forEach((name) => {
				if (processes.has(name)) {
					try {
            processes.get(name).send({ event: "cm", message: m.message, sender: characterName });
          } catch(e) {
            console.error(new Error("send_cm re-route failed, falling back to socket."));
            console.error(e);
            childProcess.send({ event: "cm_fail", name: name, message: m.message });
          };
        } else {
					childProcess.send({ event: "cm_fail", name: name, message: m.message });
				};
			});
		} else if (m.event == "script_switch") {
      switch(m.value) {
        case "jr":
          args[6] = "Jr.js";
          break;
        case "solo":
          args[6] = "Solo.js";
          break;
        case "dracul":
          args[6] = "Dracul.js";
          break;
        case "rat":
          args[6] = "Rat.js";
          break;
        case "prat":
          args[6] = "Prat.js";
          break;
        case "skeletor":
          args[6] = "Skeletor.js";
          break;
        case "xscorpion":
          args[6] = "XScorpion.js";
          break;
        case "stoneworm":
          args[6] = "StoneWorm.js";
          break;
        case "boar":
          args[6] = "Boar.js";
          break;
        case "hawk":
          args[6] = "Hawk.js";
          break;
        case "bbpompom":
          args[6] = "BBPomPom.js";
          break;
        default:
          break;
      }
    }
	});
};

const sleep = async (ms) =>  {
	return new Promise((resolve) => {
        setTimeout(resolve, ms)
    });
};

main();


