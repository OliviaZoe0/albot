smart_move("woffice", () => {
	let lost_and_found = localStorage.getItem("lostandfound_" + parent.server_region + " " + parent.server_identifier);
  parent.socket.on("lostandfound", function(data) {
        lostandfound = data;
        lostandfound.forEach((item)  => {
          if(["egg0", "egg1", "egg2", "egg3", "egg4", "egg5", "egg6", "egg7", "egg8", "goldenegg","seashell", "leather", "feather0", "midas", "gem1"].includes(item.name)) {
            parent.socket.emit("sbuy",{rid:item.rid,f:true})
          }
        });
    localStorage.setItem("lostandfound_"+parent.server_region + " " + parent.server_identifier, JSON.stringify(data));
    });
	if(lost_and_found == null) {
parent.donate(1000000);
	} else {
		parent.socket._callbacks.$lostandfound.forEach((callback) => {
			callback(lost_and_found);
		});
	}
parent.socket.emit("lostandfound");
})